import code
import sys
from io import StringIO


def test_error():
    code_with_error = """assert False, 'should be True'"""
    stderr_save = sys.stderr
    sys.stderr = new_stderr = StringIO()
    console = code.InteractiveConsole()
    console.push(code_with_error)
    sys.stderr = stderr_save
    assert new_stderr.getvalue() != ''
