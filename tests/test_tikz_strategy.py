#!/usr/bin/env python3
import os
import sys

sys.path.append("../pandoc_pyrun")
import pandoc_pf_pyrun as pfp
from panflute import Div, Image, Para, Str


def test_correct_tikz_code():
    tikz_code = """
from py_struct_vis import TikZQueue
qu = TikZQueue(69,25,74,28,63,83,49,33,79,69)
qu
"""
    st = pfp.TikZStrategy(tikz_code, pfp.SESSION_GLOBAL, "html", file='test')
    res = st.eval() == [Para(Image(url='test.png'))]
    assert res

def test_incorrect_tikz_code():
    non_tikz_code = """
42
"""
    st = pfp.TikZStrategy(non_tikz_code, pfp.SESSION_GLOBAL, "html", file='test')
    assert st.eval() == [Div(Para(Str("last expression should be a tikz figure")))]
