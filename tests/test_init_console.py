import code

import pandoc_pyrun as pp

# --------------------------------------------
# 	 Background console tests
# --------------------------------------------


def test_init_console_is_good_type():
    res = pp.init_console()
    assert type(res) is code.InteractiveConsole


def start_test():
    test_init_console_is_good_type()
    pass


if __name__ == "__main__":
    start_test()
    pass
