#!/usr/bin/env python3
import sys

sys.path.append("../pandoc_pyrun")
import pandoc_pf_pyrun as pfp
from panflute import CodeBlock, Div, Image, Para, Str

multiline_code = """
a = \"\"\"
this is a
    multiline
string
\"\"\"
for i in range(2):
    print(i)
    print(a)
"""

turtle_code = """
import turtle

def spiral(n) :
    turtle.pencolor("red")
    for i in range(n):
        turtle.forward(i * 10)
        turtle.right(144)

spiral(20)"""

plot_code = """
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()
plt.show()
"""

decorator_code = """
from functools import wraps

def trace(fct):
    '''
    Decorator for tracing every call to fct.
    Recursive calls are indented.
    '''
    @wraps(fct)
    def wrapper(*args, **kwargs):
        dots = '...' * wrapper.__depth
        print('{:s} -> {:s}{:s}'.format(dots, wrapper.__name__, repr((args, kwargs))))
        wrapper.__depth += 1
        y = fct(*args, **kwargs)
        wrapper.__depth -= 1
        print('{:s} <- {:s}'.format(dots, repr(y)))
        return y
    wrapper.__depth = 0
    return wrapper

@trace
def fact(n):
    if n == 0:
        return 1
    return n*fact(n-1)

fact(4)
"""

decorator_shell_result = """>>> from functools import wraps
>>> def trace(fct):
...     '''
...     Decorator for tracing every call to fct.
...     Recursive calls are indented.
...     '''
...     @wraps(fct)
...     def wrapper(*args, **kwargs):
...         dots = '...' * wrapper.__depth
...         print('{:s} -> {:s}{:s}'.format(dots, wrapper.__name__, repr((args, kwargs))))
...         wrapper.__depth += 1
...         y = fct(*args, **kwargs)
...         wrapper.__depth -= 1
...         print('{:s} <- {:s}'.format(dots, repr(y)))
...         return y
...     wrapper.__depth = 0
...     return wrapper
>>> @trace
... def fact(n):
...     if n == 0:
...         return 1
...     return n*fact(n-1)
>>> fact(4)
-> fact((4,), {})
... -> fact((3,), {})
...... -> fact((2,), {})
......... -> fact((1,), {})
............ -> fact((0,), {})
............ <- 1
......... <- 1
...... <- 2
... <- 6
 <- 24
24"""

multiline_out_result = """0

this is a
    multiline
string

1

this is a
    multiline
string

"""

multiline_shell_result = """>>> a = \"\"\"
... this is a
...     multiline
... string
... \"\"\"
>>> for i in range(2):
...     print(i)
...     print(a)
0

this is a
    multiline
string

1

this is a
    multiline
string"""

def test_stdout_strategy_with_multiline():
    session = pfp.SESSION_GLOBAL
    fmt = "html"
    strat =  pfp.StandardOutputStrategy(multiline_code, session, fmt)
    assert strat.eval() == [CodeBlock(multiline_out_result)]

def test_shell_strategy():
    strat = pfp.ShellStrategy(multiline_code, pfp.SESSION_GLOBAL, "html")
    assert strat.eval() == [CodeBlock(multiline_shell_result, classes=['python', 'noeval'])]

def test_turtle_strategy():
    strat = pfp.TurtleStrategy(turtle_code, pfp.SESSION_GLOBAL, "html")
    assert strat.eval() == [Div(Para(Image(url='6634422f69afeab832c17d25475b54c1fda4995d.png')),
                                classes=['turtle'])]

def test_plot_strategy():
    strat = pfp.PlotStrategy(plot_code, pfp.SESSION_GLOBAL, "html")
    assert strat.eval() == [Para(Image(url='5f049679acc4c39443bf3cb48edd416511c7228a.png'))]

def test_none_strategy():
    eval_strat =  pfp.StandardOutputStrategy(multiline_code, pfp.SESSION_GLOBAL, "html")
    export_strat = pfp.OutNone(multiline_code, eval_strat, False)
    assert export_strat.out() == []

def test_code_strategy():
    eval_strat =  pfp.StandardOutputStrategy(multiline_code, pfp.SESSION_GLOBAL, "html")
    export_strat = pfp.OutCode(multiline_code, eval_strat, False)
    assert export_strat.out() == [CodeBlock(multiline_code, classes=['py', 'noeval'])]

def test_all_strategy():
    eval_strat =  pfp.StandardOutputStrategy(multiline_code, pfp.SESSION_GLOBAL, "html")
    export_strat = pfp.OutAll(multiline_code, eval_strat, False)
    assert export_strat.out() == [CodeBlock(multiline_code, classes=['py', 'noeval']),
                                  CodeBlock(multiline_out_result, classes=['python', 'noeval'])]

def test_res_strategy():
    eval_strat =  pfp.StandardOutputStrategy(multiline_code, pfp.SESSION_GLOBAL, "html")
    export_strat = pfp.OutRes(multiline_code, eval_strat, False)
    assert export_strat.out() == [CodeBlock(multiline_out_result, classes=['python', 'noeval'])]

def test_shell_strategy_with_decorators():
    strat = pfp.ShellStrategy(decorator_code, pfp.SESSION_GLOBAL, "html")
    assert strat.eval() == [CodeBlock(decorator_shell_result, classes=['python', 'noeval'])]
