"""
Pandoc_pyrun: python code compiler from Markdown files
====================================
Pandoc_pyrun is a Python package of a `Pandoc <http://pandoc.org/>` filter.
It executes python code inside `CodeBlock`. **Code has to be trusted**.
"""
from .pandoc_pf_pyrun import *
