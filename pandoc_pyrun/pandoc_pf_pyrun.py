#!/usr/bin/env python

"""
pandoc filter to run python

:author: B. Bapegay <benoit.papegay@univ-lille.fr>
:date: jan 24
"""

from __future__ import annotations

import ast
import code
import hashlib
import sys
from abc import abstractmethod
from io import StringIO
from typing import Type

from pandocfilters import get_extension
from panflute import (CodeBlock, Div, Doc, Element, Image, LineBreak, Para,
                      Str, Strong, run_filter)

logger = []
NO_EVAL = 'noeval'
PYTHON_CLASSES = ('py', 'python')
SESSION_GLOBAL = "global"

# output strategies management
out_strategies_map: dict[str, Type[OutStrategy]] = {}


def register_out_strategy(cls):
    """register an out strategy."""
    for stid in cls.__stratID__:
        out_strategies_map[stid] = cls
    return cls

# the output strategies


class OutStrategy:
    """
    base class of the out strategies.
    An out strategy try to answer the question
    'how the code block and its evaluation are mixed ?'
    ie : nothing, only the code, only the results, everything
    """

    __stratID__: list[str]

    """A class representing the out strategy."""
    def __init__(self,
                 code: str,
                 res: ResultStrategy,
                 noeval: bool):
        self._code = code
        self._result = res
        self._noeval = noeval

    @abstractmethod
    def out(self) -> list[Element]:
        ...


@register_out_strategy
class OutNone(OutStrategy):
    """
    A class representing the None strategy : nothing is outputed."""
    __stratID__ = ["none"]

    def out(self) -> list[Element]:
        """nothing is outputed"""
        if not self._noeval:
            self._result.eval()
        return []


@register_out_strategy
class OutCode(OutStrategy):
    """A class representing the code strategy : only the code is outputed"""
    __stratID__ = ["code"]

    def out(self) -> list[Element]:
        """block eval is returned"""
        if not self._noeval:
            self._result.eval()
        return [CodeBlock(self._code, classes=["py", "noeval"])]


@register_out_strategy
class OutRes(OutStrategy):
    """A class representing the results strategy : only the result is outputed."""
    __stratID__ = ["res", "shell"]

    def out(self) -> list[Element]:
        """return mixed string of cmd and results."""
        return self._result.eval() if not self._noeval else []


@register_out_strategy
class OutAll(OutStrategy):
    """A class representing the all strategy : code and result are outputed."""
    __stratID__ = ["all"]

    def out(self) -> list[Element]:
        """code block and evaluation are outputed."""
        return [CodeBlock(self._code, classes=["py", "noeval"])] + \
            self._result.eval() if not self._noeval else []


# evaluation management
result_strategies_map: dict[str, Type[ResultStrategy]] = {}


def register_result_strategy(cls):
    result_strategies_map[cls.__stratID__] = cls
    return cls


class ResultStrategy:
    """
    base class of all results strategy.

    A result strategy should specify how the results are procuced
    from the code block.
    for example, the std output, a value, a drawing, ...
    """
    __stratID__: str

    def __init__(self,
                 code: str,
                 session: str,
                 fmt: str,
                 **optns):
        """initialize a new result strategy."""
        self._parseerror: bool = False
        self._log: list[str] = []
        try:
            self._codelines = []
            for stmt in ast.parse(code).body:
                if isinstance(stmt, ast.FunctionDef) or \
                   isinstance(stmt, ast.AsyncFunctionDef) or \
                   isinstance(stmt, ast.ClassDef):
                    decorators = [ast.get_source_segment(code, expr)
                                  for expr in stmt.decorator_list]
                else:
                    decorators = []
                srcstmt = ast.get_source_segment(code, stmt)
                if srcstmt is not None:
                    decoration = "\n".join(f"@{dec}"
                                           for dec in decorators)
                    if len(decoration) > 0:
                        srcstmt = f"{decoration}\n{srcstmt}"
                    self._codelines.append(srcstmt.rstrip())
        except:
            self._codelines = code.splitlines()
            self._parseerror = True

        self._fmt = fmt
        if session not in eval_sessions:
            eval_sessions[session] = new_console([])
        self._session = eval_sessions[session]
        # options
        self._file = optns.get('file', None)
        if self._file is not None and "." in self._file:
            self._file, _, _ = self._file.rpartition(".")

        # save std out and err
        self._stdout = sys.stdout
        self._stderr = sys.stderr

    def image_path(self) -> str:
        """generate an image name from stmt."""
        stmt = "".join(self._codelines)
        ext = get_extension(self._fmt, 'png',
                            html='png', latex='pdf')
        if self._file is None:
            sha = hashlib.sha1(stmt.encode(sys.getfilesystemencoding()))
            impath = "{}.{}".format(sha.hexdigest(), ext)
        else:
            impath = f"{self._file}.{ext}"
        return impath

    def eval(self) -> list[Element]:
        """evaluate the block."""
        if self._parseerror:
            return [Div(Para(Str("parsing "), Str("error")))]
        return self._eval()

    def restore_stdout(self):
        """
        restore system std out and err.
        """
        sys.stdout = self._stdout
        sys.stderr = self._stderr

    @classmethod
    def change_stdout(cls, changeStdErr: bool = False) -> tuple[StringIO, StringIO]:
        """
        create a new io string and set std out and err
        accordingly.
        """
        new_stdout = StringIO()
        new_stderr = StringIO() if changeStdErr else new_stdout
        sys.stdout = new_stdout
        sys.stderr = new_stderr
        return new_stdout, new_stderr

    @abstractmethod
    def _eval(self) -> list[Element]:
        ...


@register_result_strategy
class StandardOutputStrategy(ResultStrategy):
    """
    Collect the results procuced on the std output during the code block evaluation.
    """
    __stratID__ = "out"

    def _eval(self) -> list[Element]:
        """
        eval the code and produce the results.
        ie concatenation of outputs.
        """
        new_stdout, _ = ResultStrategy.change_stdout()
        for line in self._codelines:
            if self._session.push(line):
                self._session.push("\n")
        self.restore_stdout()
        return [CodeBlock(new_stdout.getvalue())]


@register_result_strategy
class ShellStrategy(ResultStrategy):
    """
    The results is a mix of code lines and its evaluation.
    """
    __stratID__ = "shell"

    def _eval(self) -> list[Element]:
        """
        eval the code an produce the results.
        ie concatenation of outputs.
        """
        res = []
        for line in self._codelines:
            new_stdout, _ = ResultStrategy.change_stdout()
            if self._session.push(line):
                self._session.push('\n')
            sublines = line.splitlines()
            line = ">>> " + "\n... ".join(sublines)
            lineout = new_stdout.getvalue().strip()
            if lineout != '':
                res.append(f"{line}\n{lineout}")
            else:
                res.append(f"{line}")
        self.restore_stdout()
        return [CodeBlock("\n".join(res),
                          classes=["python", "noeval"])]


@register_result_strategy
class TurtleStrategy(ResultStrategy):
    """Represents the evaluation of a turtle bloc"""
    __stratID__ = "turtle"

    PREAMBULE = ["import turtle",
                 "import PIL.Image",
                 "\n".join(["def turtle2image(impath: str):",
                            "    turtle.getscreen().getcanvas().postscript(file='tmp.eps')",
                            "    fig = PIL.Image.open('tmp.eps')",
                            "    fig.save(impath, lossless=True)"])]

    def _eval(self) -> list[Element]:
        """
        eval the code an produce the results.
        ie concatenation of outputs.
        """
        _, _ = ResultStrategy.change_stdout()
        impath = self.image_path()
        for line in self.PREAMBULE + self._codelines:
            self._log.append(line)
            if self._session.push(line):
                self._session.push('\n')
        self._session.push(f"turtle2image(\'{impath}\')")
        self.restore_stdout()
        return [Div(Para(Image(url=impath)),
                    classes=["turtle"])]


@register_result_strategy
class PlotStrategy(ResultStrategy):
    """Represents the evaluation of a plot code"""
    __stratID__ = "plot"

    def _eval(self) -> list[Element]:
        """
        replace the show() code by a savefig() one with
        a known filename and then produce an image block
        of its name.
        """
        new_stdout, _ = ResultStrategy.change_stdout()
        res = []
        waiting: bool = False
        impath = self.image_path()
        try:
            for line in self._codelines:
                if not waiting and line.endswith(".show()"):
                    line = line.replace(".show()",
                                        f".savefig('{impath}', bbox_inches='tight')")
                    waiting = self._session.push(line)
                    res.append(Image(url=impath))
                else:
                    waiting = self._session.push(line)
                if waiting:
                    self._session.push('\n')
        except:
            self._log.append(new_stdout.getvalue())
        self.restore_stdout()
        if len(res) > 0:
            # return [Div(
            #     Para(*res),
            #     classes = ["plot"])]
            return [Para(*res)]

        else:
            return [Div(
                Para(LineBreak(), Str("Command "),
                     Strong(Str(".show()")), Str(" not found."))
            )]


@register_result_strategy
class PilStrategy(ResultStrategy):
    """Represents the evaluation of a turtle bloc"""
    __stratID__ = "pil"

    PREAMBULE = ["import PIL.Image"]

    def _eval(self) -> list[Element]:
        """
        eval the code an produce the results.
        ie concatenation of outputs.
        """
        new_stdout, _ = ResultStrategy.change_stdout()
        res = []
        waiting: bool = False
        impath = self.image_path()
        try:
            for line in self.PREAMBULE + self._codelines:
                if not waiting and line.endswith(".show()"):
                    line = line.replace(".show()",
                                        f".save('{impath}', 'PNG')")
                    waiting = self._session.push(line)
                    res.append(Image(url=impath))
                else:
                    waiting = self._session.push(line)
                if waiting:
                    self._session.push('\n')
        except:
            self._log.append(new_stdout.getvalue())
        self.restore_stdout()
        if len(res) > 0:
            return [Para(*res)]
        else:
            return [Div(
                Para(LineBreak(), Str("Command "),
                     Strong(Str(".show()")), Str(" not found."))
            )]


@register_result_strategy
class TikZStrategy(ResultStrategy):
    """evaluation of a block with TikZ objects."""
    __stratID__ = "tikz"

    def _eval(self) -> list[Element]:
        """
        eval the code an produce the results.
        ie concatenation of outputs.
        """
        impath = self.image_path()
        _, new_stderr = ResultStrategy.change_stdout(False)
        self._session.push("from py_struct_vis.tikz import TikZ, AbstractTikZFigure")
        if new_stderr.getvalue() != '':
            res = [Div(Para(Str("import error. Is py-struct-vis installed ?")))]
        else:
            for line in self._codelines[:-1]:
                if self._session.push(line):
                    self._session.push("\n")
            last = self._codelines[-1]
            ws, expr, _ = last.partition(last.lstrip())
            _, new_stderr = ResultStrategy.change_stdout(False)
            self._session.push(f"assert isinstance({expr.strip()}, AbstractTikZFigure)")
            if new_stderr.getvalue() != '':
                res = [Div(Para(Str("last expression should be a tikz figure")))]
            else:
                if self._session.push(ws + f"TikZ({expr.strip()},filename='{impath}')"):
                    self._session.push("\n")
                res = [Para(Image(url=impath))]
        self.restore_stdout()
        return res


# session managements

def new_console(init_lines: list[str]) -> code.InteractiveConsole:
    """
    return a new console, initialize with initlines lines
    (without trailing \n)
    """
    res = code.InteractiveConsole()
    for line in init_lines:
        res.push(line)
    return res


eval_sessions: dict[str, code.InteractiveConsole] = {
    SESSION_GLOBAL: new_console([])
}


class PyBloc:
    """represents a python code bloc."""

    def __init__(self, code: str, fmt: str,
                 *classes: list[str],
                 **parameters):
        """
        Initialize a new python code bloc.
        """
        # retrieve session
        session = parameters.get("session", SESSION_GLOBAL)
        if session not in eval_sessions:
            eval_sessions[session] = new_console([])
        self._session = eval_sessions[session]
        # retrieve out strat
        out = parameters.get("export",
                             parameters.get("out", "all"))
        if out not in out_strategies_map:
            out = 'all'
        out_strat_cls = out_strategies_map[out]
        # retrieve evaluation strat
        res = parameters.get("eval",
                             parameters.get("res", "out"))
        if res not in result_strategies_map:
            res = "out"
        res_strat_cls = result_strategies_map[res]
        # retrieve filename
        fname = parameters.get("file", None)
        # initialise strategies
        self._resultstrategy = res_strat_cls(code, session,
                                             fmt, file=fname)
        self._outstrategy = out_strat_cls(code, self._resultstrategy,
                                          'noeval' in classes)

    def out(self) -> list[Element]:
        return self._outstrategy.out()


def pyrun(elem: Element, doc: Doc):
    """
    add content to documentation block
    """
    if isinstance(elem, CodeBlock):
        if any(cl in elem.classes for cl in PYTHON_CLASSES):
            pybloc = PyBloc(elem.text,
                            doc.format,
                            *elem.classes,
                            **elem.attributes)
            return pybloc.out()


def prepare(doc):
    # read defaults value for out and res from yaml
    pass


def finalize(doc):
    pass


def main(doc=None):
    res = run_filter(pyrun,
                     prepare=prepare,
                     finalize=finalize,
                     doc=doc)
    if logger != []:
        with open('log.txt', 'w') as fout:
            fout.write('\n'.join(str(line) for line in logger))
    return res


if (__name__ == "__main__"):
    main()
