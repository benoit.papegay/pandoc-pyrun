from setuptools import find_namespace_packages, setup

setup(
    name="pandoc_pyrun",
    version="1.0.2",
    description="Pandoc filter to run python code blocks",
    url="https://gitlab-fil.univ-lille.fr/benoit.papegay/pandoc-pyrun",
    download_url="https://gitlab-fil.univ-lille.fr/benoit.papegay/pandoc-pyrun/-/archive/master/pandoc-pyrun-master.tar.gz",
    author="Benoit Papegay, D'hulst Thomas, Tayebi Ajwad",
    author_email="benoit.papegay@univ-lille.fr",
    license="BDS2",
    packages=find_namespace_packages(),
    python_requires='>=3.6,<4',
    install_requires=["pandocfilters", "matplotlib"],
    keywords="pandoc filters markdown python notes pandocfilters pdf latex",
    py_modules=["pandoc_pyrun.pandoc_pf_pyrun"],

    entry_points={
        "console_scripts": [
            "pandoc_pyrun = pandoc_pyrun.pandoc_pf_pyrun:main",
        ],
    },
    extras_require={
        "dev": ["check-manifest"],
        "test": ["coverage"],
    },
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "coverage"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",  # to be reviewed
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Documentation",
        "Topic :: Text Processing :: Filters",
    ]
)
