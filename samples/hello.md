---
title: Hello world sample
author: TayebiDhulst
date: March 24, 2021
---

# Fichier

```{.py export=all}
def say_hello():
    print("hi there\nhow u doin?")

say_hello()
```
