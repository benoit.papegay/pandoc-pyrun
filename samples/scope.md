---
title: Scope's sample
author: TayebiDhulst
date: February 15, 2021
---

# Scope

By default the *scope* option is set to **GLOBAL** and each function is stored in memory.

However, if we want to compile code in a *local* view and not add it to other code, we can use the *scope=LOCAL* option.

```{.python export=none}
def plusUn(x):
    return x+1
```

```{.python export=all session=demo}
def plusUn(x):
    return x+2
plusUn(2)
```

```{.python export=all}
# evaluation dans la session globale
plusUn(2)
```

```{.python export=all session=demo}
# evaluation dans la session demo
plusUn(2)
```
