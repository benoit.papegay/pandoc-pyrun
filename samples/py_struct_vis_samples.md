---
title: samples using py_struct_vis
author: B. Papegay
---

you can download experimental version of `py-struct-vis` here, install it with pip :

```
pip install py_struct_vis-0.1.0-py3-none-any.whl --break-system-packages
```

# stack examples

To run theses examples you must have installed py-struct-vis

```{.py export=none}
from py_struct_vis import *
```


```{.py eval=tikz export=both}
st = TikZStack()
for d in (3, 1, 4, 1, 5):
    st.push(d) 
st
```

convert this document with

```{.sh}
 pandoc -t markdown -o py_struct_vis_samples-out.md \
     -F ../pandoc_pyrun/pandoc_pf_pyrun.py \
     -L ./columns.lua py_struct_vis_samples.md 
 ```

or in pdf :

```{.sh}
 pandoc -t pdf -o py_struct_vis_samples-out.pdf \
     -F ../pandoc_pyrun/pandoc_pf_pyrun.py \
     -L ./columns.lua py_struct_vis_samples.md 
 ```

you should see the stack ...

```{.py eval=shell export=res}
st
print(st)
```

differents stack state :

::: columns :::

:::: column ::::

```{.py eval=shell export=res}
st.pop()
```

```{.py eval=tikz export=resfile=etat1.png}
st
```

::::

:::: column ::::

```{.py eval=shell export=res}
st.pop()
```

```{.py eval=tikz export=res file=etat2}
st
```

::::

:::: column ::::

```{.py eval=shell export=res}
st.pop()
```

```{.py eval=tikz export=res file=etat3}
st
```

::::

:::

# queue examples

A new queue can be created with the TikZQueue class :

```{.py eval=shell export=res}
qu = TikZQueue()
for d in (3, 1, 4, 1, 5):
    qu.enqueue(d) 
```

```{.py eval=tikz export=res file=q1}
qu
```

```{.py eval=shell export=res}
qu.dequeue()
qu.is_empty()
```

```{.py eval=tikz export=res file=q2}
qu
```

```{.py eval=shell export=res}
qu.dequeue()
qu.dequeue()
```

```{.py eval=tikz export=res file=q3}
qu
```
