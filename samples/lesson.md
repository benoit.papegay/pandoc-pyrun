---
title: Lesson example
author: TayebiDhulst
date: February 15, 2021
---

# Lesson example

## Python lesson

Make a predicate *for_all* parameterized by an iterable that returns **True** if
and only if all elements of the iterable passed as a parameter are true and
**False** otherwise.

Exemple :

```{.python export=none}
def for_all(liste):
    return not False in liste
```

```{.python export=all}
for_all([])
for_all((True, True, True))
for_all((True, False, True))
```
