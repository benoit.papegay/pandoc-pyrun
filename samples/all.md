# First sample

# Invisibility

```{.python export=none}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.python export=all}
couples(['a', 'b', 'c'])
#that's a commentary
```
---

```{.python export=code}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.python export=res}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#that's a commentary
```

---

```{.py export=shell}
couples(['a', 'b', 'c'])
couples(['a', 'b', 'c'])
couples(['a', 'b', 'c'])
couples(['a', 'b', 'c'])
```

# Session

By default the *session* option is set to `"global"` and each function is stored in memory.

However, if we want to run the script code in another sessionm  view and not add it to other code, we can use the *session=session_name* option, where session_name is the name of the session. Differents sessions can run accross the document. 

```{.python export=none}
def plusUn(x):
    return x+1
```

```{.python export=all session=demo}
def plusUn(x):
    return x+2
plusUn(2)
```

```{.python export=all}
plusUn(2)
```

```{.python export=all session=demo}
plusUn(2)
```

# File

To include file, just import it. If you want to show code source or function signature, I suggest using pydoc filter.

# Lesson example

## Python lesson

Make a predicate *for_all* parameterized by an iterable that returns **True** if and only if all elements of the iterable passed as a parameter are true and **False** otherwise.

Exemple :

```{.python export=none}
def for_all(liste):
    return not False in liste
```

```{.python export=shell}
for_all([])
for_all((True, True, True))
for_all((True, False, True))
```

# Double Div, Error, rafters, .py, .python and .pandocPyrun

```{.pandocPyrun export=all}
1+1
```

```{.py export=all}
def salut(x):
    return x

salut("yo")
"" + 3
```

# Mathplot

```{.py export=all eval=plot}
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()
plt.show()
```
# Turtle star

```{.python eval=turtle export=res}
import turtle

def spiral(n) :
    turtle.pencolor("red")
    for i in range(n):
        turtle.forward(i * 10)
        turtle.right(144)

spiral(20)
```

```{.python eval=turtle export=none}
turtle.forward(160)
```
