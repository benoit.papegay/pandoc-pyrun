---
title: Invisibility's sample
author: TayebiDhulst
date: February 15, 2021
out: ALL
---

# Invisibilité

```{.python export=none}
def couples(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.python export=all}
def couples1(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```
---

```{.python export=code}
def couples2(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

couples(['a', 'b', 'c'])
#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.python export=res}
def couples3(l):
    t = []
    for i, e in enumerate(l) :
        t.append((i, e))
	return t

print(f"appel à couple3 :{couples3(['a', 'b', 'c'])}")

#ceci es tun commenfmoiazuhqgpmofrhezbprhyuighveipsulvbruflk
```

---

```{.py export=res eval=shell}
couples(['a', 'b', 'c'])
couples1(['a', 'b', 'c'])
couples2(['a', 'b', 'c'])
couples3(['a', 'b', 'c'])
```
