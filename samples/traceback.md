---
title: Traceback and rafters sample
author: TayebiDhulst
date: February 15, 2021
---

# Double Div, Error, rafters, .py, .python and .pandocPyrun

```{.py export=all}
1+1
```

```{.py export=all}
def salut(x):
    return x

salut("yo")
"" + 3
```
