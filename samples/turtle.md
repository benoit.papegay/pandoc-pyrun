---
title: Turtle's sample
author: TayebiDhulst
date: March 24, 2021
out: ALL
---
# Turtle star

```{.python res=turtle out=res}
import turtle

def spiral(n) :
    turtle.pencolor("red")
    for i in range(n):
        turtle.forward(i * 10)
        turtle.right(144)

spiral(20)
```

```{.python res=turtle out=none}
turtle.forward(160)
```
