# Tutorial
The goal of this tutorial is to understand as easy and as much as possible how to use the filter, all the parts of the "script" that you'll find below will be gathered in the [tutorial_script](tutorial_script.md).  
\
\

# Important
Make sure you've read [the installation guide](../README.md#Installation) before starting to read this.  
\
\


# Beginning
Let's imagine we have an exercise, typical for exams, like this :  

> **Exercise 1 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mark is a young boy who wants to organize a birthday with his friends and he wants to suggest an original game to them. Here are the rules :  
> - They all pick a number from 0 to 100 *[a number can't be picked more than once]*  
> - In a jar, we put one paper per number, this number written on it  
> - We draw one paper per one paper :  
>   - If the drawn number has been chosen by someone, (s)he loses the game  
>   - If it's not, we do nothing and keep drawing   
> - When there are two persons remaining, we make a coin toss to determine the winner   
> 
> **NB :** They have to be at least 2 to play this game.  


\
\


After reading this paragraph, we can think about functions that will be written by the students :
- run a toss coin,  
- check if an element is in a list,  
- remove an element from a list, if contained in it,  
- pop a random element from a list.  
- ...  


\


Then, you can think about a bunch of questions :  
> **Question 1 :** *Write a function `tossCoin()` that returns 0 (for heads) or 1 (for tails).*  
> **Question 2 :** *Write a function `checkElement(elt, lst)` that returns True if elt is in lst, False otherwise.*  
> **Question 3 :** *Write a function `removeElement(elt, lst)` that returns lst, with elt removed if contained in it, unchanged otherwise.*  
> **Question 4 :** *Write a function `popElement(lst)` that returns a random element from lst and lst without this random element.*  
> **Question 5 :** *Write a function `virtuosoRoulette(players)` that takes the list of all the chosen numbers and returns the one that has not been eliminated, -1 if something is wrong.*  


\
\


Well now, you're the writer so let's go ! It's time to write !  


\


First, on the top of the file, you can define meta data to simplify your writing.
```
---
title: Some University Exam Exercises
author: Your name
date: April 24, 2021
pandoc_pyrun_mode: STUDENT
pandoc_pyrun_out : RES
pandoc_pyrun_type : PY
---
```  
So we consider here that the generated file will be for a STUDENT, which means all indicated classes' values will be respected and we put as default the values CODE and PY respectively to out and type, which means only the result is printed without printing any code and this is pure native code, no plot or drawing to print.  


\
\


Then, you can think about creating a `utils.py` file that will contains all the necessary imports for the incoming functions.  
```python
import random
import os
import sys
import turtle
```  
If you use a file for your imports, you can put this CodeBlock, just after the YAML header :  
```
  ```{.py out=NONE file=utils.py}
    ```
```   
This will import the content of the file `utils.py` and compile it until the end of the filter use. But there, we'll just put the raw content in a similar CodeBlock.  
```python
  ```{.py out=NONE}
    import random
    import os
    import sys
    import turtle
    ```
``` 


\
\


Now it's time to start writing the questions and see how can the filter be really useful for some questions !  
Let's come back to our main instruction with the first question.  


> **Exercise 1 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mark is a young boy who wants to organize a birthday with his friends and he wants to suggest an original game to them. Here are the rules :  
> - They all pick a number from 0 to 100 *[a number can't be picked more than once]*  
> - In a jar, we put one paper per number, this number written on it  
> - We draw one paper per one paper :  
>   - If the drawn number has been chosen by someone, (s)he loses the game  
>   - If it's not, we do nothing and keep drawing   
> - When there are two persons remaining, we make a coin toss to determine the winner   
> 
> **NB :** They have to be at least 2 to play this game.  
>   
> **Question 1 :** *Write a function `tossCoin()` that returns 0 (for heads) or 1 (for tails).*  
```py
    ```{.py out=NONE}
    def tossCoin():
        return random.randint(0,1)
    ```
    ```{.py out=SHELL}
    >>> [tossCoin() for i in range(15)]
    ```
```  
Here, this is what you, the writer will see, but if we make a point on all the variables defined at this moment, we have :  
- **out = SHELL** *(-> defined here - like a shell, print an exec and its result, etc.)*  
- **type = PY**   *(-> defined in META - print the result of the code)*  
- **scope = GLOBAL** *(-> defined by default in the filter - will be compiled until the end of the filter)*  

This will give you a visual render like this :  
```py
>>> [tossCoin() for i in range(15)]
[1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1]
```


\
\


And you can keep going like this for all the following questions !  
Imagine you write this :  


> **Question 2 :** *Write a function `checkElement(elt, lst)` that returns True if elt is in lst, False otherwise.*  
```py
    ```{.py out=NONE}
    def checkElement(elt, lst):
        return elt in lst
    ```
    ```{.py out=SHELL}
    >>> checkElement(1, [1,2,3,4])
    >>> checkElement("a", ["n", "c", "b"])
    >>> checkElement(True, [False, False, not False, False])
    ```
```  
> **Question 3 :** *Write a function `removeElement(elt, lst)` that returns lst, with elt removed if contained in it, unchanged otherwise.*  
```py
    ```{.py out=NONE}
    def removeElement(elt, lst):
        if checkElement(elt, lst):
            lst.remove(elt)
        return lst
    ```
    ```{.py out=SHELL}
    >>> removeElement(1, [1,2,3,4])
    >>> removeElement("a", ["n", "c", "b"])
    >>> removeElement(True, [False, False, not False, False])
    >>> removeElement(5, [5, 10, 15, 5, 30])
    ```
``` 
> **Question 4 :** *Write a function `popElement(lst)` that returns a random element from lst and lst without this random element.*  
```py
    ```{.py out=NONE}
    def popElement(lst):
        i = random.randint(0, len(lst) - 1)
        return lst.pop(i), lst
    ```
    ```{.py out=SHELL}
    >>> popElement([1,2,3,4])
    >>> popElement(["n", "c", "b"])
    ```
``` 
> **Question 5 :** *Write a function `virtuosoRoulette(players)` that takes the list of all the chosen numbers and returns the one that has not been eliminated, -1 if something is wrong.*  
```py
    ```{.py out=NONE}
    def virtuosoRoulette(players):
        if len(players) < 2 or not all((type(nbr) == int) and (0 <= nbr <= 100) for nbr in players): return -1
        
        nbrs = [i for i in range(101)]
        while len(players) > 2 :
            elt, nbrs = popElement(nbrs)
            players = removeElement(elt, players)
            
        return players[0] if tossCoin() == 0 else players[1]
    ```
    ```{.py out=SHELL}
    >>> virtuosoRoulette([55, 15, 67, 99, 1, 5])
    >>> virtuosoRoulette([55, 15, 67, "99", 1, 5])
    >>> virtuosoRoulette([55, 15, 670, 99, 1, 5])
    ```
``` 


\
\


By writing this, you'll get a render like this :  
> **Question 2 :** *Write a function `checkElement(elt, lst)` that returns True if elt is in lst, False otherwise.*  
```py
>>> checkElement(1, [1,2,3,4])
True
>>> checkElement("a", ["n", "c", "b"])
False
>>> checkElement(True, [False, False, not False, False])
True
```  
> **Question 3 :** *Write a function `removeElement(elt, lst)` that returns lst, with elt removed if contained in it, unchanged otherwise.*  
```py
>>> removeElement(1, [1,2,3,4])
[2, 3, 4]
>>> removeElement("a", ["n", "c", "b"])
["n", "c", "b"]
>>> removeElement(True, [False, False, not False, False])
[False, False, False]
>>> removeElement(5, [5, 10, 15, 5, 30])
[10, 15, 5, 30]
```  
> **Question 4 :** *Write a function `popElement(lst)` that returns a random element from lst and lst without this random element.*  
```py
>>> popElement([1,2,3,4])
(2, [1, 3, 4])
>>> popElement(["n", "c", "b"])
("c", ["n", "b"])
```  
> **Question 5 :** *Write a function `virtuosoRoulette(players)` that takes the list of all the chosen numbers and returns the one that has not been eliminated, -1 if something is wrong.* 
```py
>>> virtuosoRoulette([55, 15, 67, 99, 1, 5])
67
>>> virtuosoRoulette([55, 15, 67, "99", 1, 5])
-1
>>> virtuosoRoulette([55, 15, 670, 99, 1, 5])
-1
``` 


\
\


Now, let's try some turtle trace :  
> **Exercise 2 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Below, you'll find a turtle trace with the associated code.      
```py
    ```{.py out=ALL type=TURTLE}
    def hexagon():
        turtle.speed(0)
        turtle.pencolor("purple")
        for i in range(6):
            turtle.forward(90)
            turtle.right(60)

    hexagon()
    ```
``` 
> **Question 1 :** *Write a function `cross()` that, starting from the trace above, will give you this trace.*  
```py
    ```{.py out=NONE}
    def cross():
        turtle.speed(0)
        turtle.right(60)
        turtle.forward(180)
        turtle.backward(180)
        
        turtle.left(60)
        turtle.forward(90)
        
        turtle.right(120)
        turtle.forward(180)
        turtle.backward(180)
        
        turtle.left(60)
        turtle.forward(90)
        
        turtle.right(120)
        turtle.forward(180)
        turtle.backward(180)
    ```
    ```{.py out=ALL type=TURTLE}
        cross()
    ```
``` 
Here, this is what you, the writer will see, but if we make a point on all the variables defined at this moment, we have :  
- **out = ALL** *(-> defined here - will print all the content written plus the result of its execution)*  
- **type = TURTLE**   *(-> defined here - print the turtle trace resulting from the code)*  
- **scope = GLOBAL** *(-> defined by default in the filter - will be compiled until the end of the filter)*  


\
\


By writing this, you'll get a render like this :  
> **Exercise 2 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Below, you'll find a turtle trace with the associated code.      
```py
def hexagon():
    turtle.speed(0)
    turtle.pencolor("purple")
    for i in range(6):
        turtle.forward(90)
        turtle.right(60)

hexagon()
``` 
![Hexagon](resources/hexagon.png)
> **Question 1 :** *Write a function `cross()` that, starting from the trace above, will give you this trace.*  
```py
cross()
```
![Cross](resources/cross.png)


\
\


By the end, let's make a plot :  
> **Exercise 3 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proove that your previous function `tossCoin()` is right by showing multiple results of its execution in a plot.  
> The expected plot should look like this :   
```py
    ```{.py out=RES type=PLOT}
    import matplotlib
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    lst = [tossCoin() for i in range(1000)]
    occu = [lst.count(0), lst.count(1)]
    nbrs = ['0', '1']
    ax.bar(nbrs, occu)
    plt.show()
    ```
``` 


\
\


By writing this, you'll get a render like this :  
> **Exercise 3 :**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proove that your previous function `tossCoin()` is right by showing multiple results of its execution in a plot.  
> The expected plot should look like this :   
![tossCoin Stats](resources/tossCoin.png)
